////////////////////
// Mateo Agustoni //
// 54461	  //
///////////////////

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(){
	int fd;
	char buf[2];
	
	if(mkfifo("/tmp/FIFO_L", 0666) < 0){
		perror("Error creacion FIFO_L");
		exit(1);
	}

	if((fd=open("/tmp/FIFO_L", O_RDONLY)) < 0){
		perror("Error apertura FIFO_L");
		exit(1);
	}	

	while(1){
		if(read(fd, buf, 2) < 2){
			perror("Error lectura FIFO_L");
			exit(1);
		}
		
		if(buf[1]=='9'){
			write(1, "Fin del juego\n", sizeof("Fin del juego\n"));
			if(buf[0]=='1' || buf[0]=='2'){
				write(1, "Gana el jugador ", sizeof("Gana el jugador "));
				write(1, &buf[0], 1);
				write(1, "\n", 1);
			}
		
			close(fd);
			unlink("/tmp/FIFO_L");
			exit(0);	
		}
		
		write(1, "Jugador ", sizeof("Jugador "));
		write(1, &buf[0], 1);
		write(1, " marca 1 punto, lleva un total de ", 
			sizeof(" marca 1 punto, lleva un total de "));
		write(1, &buf[1], 1);
		write(1, " punto/s\n", sizeof(" punto/s\n"));
	}
}
