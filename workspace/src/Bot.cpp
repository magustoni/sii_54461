////////////////////
// Mateo Agustoni //
// 54461	  //
///////////////////

#include "DatosMemCompartida.h"
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>


int main(){
	DatosMemCompartida *pData;
	int fd;

	if((fd=open("/tmp/datos.txt", O_RDWR)) < 0){
		perror("Error fichero datos bot");
		exit(1);
	}
	
	if((pData = (DatosMemCompartida*)mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0)) == MAP_FAILED){
		perror("Error mmap bot");
		exit(1);
	}
	close(fd);
	
	while(1){
		if(pData->accion==9){
			munmap(pData, sizeof(DatosMemCompartida));
			exit(0);
		}
		if((pData->raqueta1.y1+pData->raqueta1.y2)/2 > pData->esfera.centro.y) pData->accion=-1;
		if((pData->raqueta1.y1+pData->raqueta1.y2)/2 < pData->esfera.centro.y) pData->accion=1;
		usleep(10000);
	}
}
